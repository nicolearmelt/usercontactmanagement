﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.DTO
{
    public class AddressDto
    {
        public int AddressId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Notes { get; set; }
        public string AddressType { get; set; }

    }
}
