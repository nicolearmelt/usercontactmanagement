﻿using AutoMapper;
using DR1.DTO;
using DR1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserAddressDTO>()
                .ForMember(destination => destination.Id, options => options.MapFrom(source => source.UserId))
                .ForMember(destination => destination.FullName,
                        options => options.MapFrom(source => source.FirstName + " " + source.LastName))
                .ForMember(destination => destination.BillingAddress, options => options.MapFrom(
                    source => source.Address.Where(x => x.AddressType == "billing").Select(x => x.Address + " " + x.City + " " + x.PostalCode).FirstOrDefault()))
                .ForMember(destination => destination.DeliveryAddress, options => options.MapFrom(
                    source => source.Address.Where(x => x.AddressType == "delivery").Select(x => x.Address + " " + x.City + " " + x.PostalCode).FirstOrDefault()));
            CreateMap<Location, AddressDto>();
        }
    }
}
