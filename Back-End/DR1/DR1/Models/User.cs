﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2,
        ErrorMessage = "First Name minimum length is 2")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2,
        ErrorMessage = "Last Name minimum length is 2")]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string MobileNumber { get; set; }
        public string Gender { get; set; }
        public ICollection<Location> Address { get; set; } = new List<Location>();
    }
}
