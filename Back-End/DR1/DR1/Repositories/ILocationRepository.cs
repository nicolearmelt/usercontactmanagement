﻿using DR1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Repositories
{
    public interface ILocationRepository
    {
        Task<Location> CreateLocation(Location location);
        Task<List<Location>> ReadLocations();
        Task<Location> ReadLocation(string id);
        Task<Location> UpdateLocation(Location location);
        Task<bool> DeleteLocation(string id);
    }
}
