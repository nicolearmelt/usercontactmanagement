﻿using DR1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Repositories
{
    public interface IUserRepository
    {
        //Task<List<User>> GetAllUsers(int count, string orderBy, string sort, int page);
        Task<User> CreateUser(User user);
        Task<List<User>> ReadUsers();
        Task<User> ReadUser(string id);
        Task<User> UpdateUser(User user);
        Task<bool> DeleteUser(string id);
    }
}
