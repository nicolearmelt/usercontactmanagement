﻿using DR1.Context;
using DR1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Repositories
{
    public class LocationRepository : ILocationRepository
    {
        private readonly AppDbContext _dbContext;
        public LocationRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Location> CreateLocation(Location location)
        {
            try
            {
                _dbContext.Location.Add(location);
                await _dbContext.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return location;
        }

        public async Task<bool> DeleteLocation(string id)
        {
            var getId = _dbContext.Location.Where(x => x.AddressId.ToString() == id).First();
            _dbContext.Location.Remove(getId);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<Location> UpdateLocation(Location Location)
        {
            _dbContext.Location.Update(Location);
            await _dbContext.SaveChangesAsync();
            return Location;
        }

        //public async Task<List<Location>> GetAllLocations(int count, string orderBy="id",string sort="asc", int page = 1)
        //{
        //    var total = _dbContext.Location.AsNoTracking().ToList().Count;
        //    var item = await _dbContext.Location.Skip((page - 1) * count).Take(count).ToListAsync();
        //    return item;
        //}

        public async Task<List<Location>> ReadLocations()
        {
            var getAllLocations = await _dbContext.Location.ToListAsync();
            return getAllLocations;
        }

        public Task<Location> ReadLocation(string id)
        {
            throw new NotImplementedException();
        }
    }
}
