﻿using DR1.Context;
using DR1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _dbContext;
        public UserRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<User> CreateUser(User user)
        {
            _dbContext.User.Add(user);
            await _dbContext.SaveChangesAsync();
            return user;
        }

        public async Task<bool> DeleteUser(string id)
        {
            var getId = _dbContext.User.Where(x => x.UserId.ToString() == id).First();
            _dbContext.User.Remove(getId);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<User> UpdateUser(User user)
        {
            _dbContext.User.Update(user);
            await _dbContext.SaveChangesAsync();
            return user;
        }

        //public async Task<List<User>> GetAllUsers(int count, string orderBy="id",string sort="asc", int page = 1)
        //{
        //    var total = _dbContext.User.AsNoTracking().ToList().Count;
        //    var item = await _dbContext.User.Skip((page - 1) * count).Take(count).ToListAsync();
        //    return item;
        //}

        public async Task<List<User>> ReadUsers()
        {
            var getAllUsers = await _dbContext.Location
                                .Join(_dbContext.User,
                                    location => location.UserId,
                                    user => user.UserId,
                                    (location, user) => user).Include(x => x.Address).ToListAsync();
            return getAllUsers;
        }

        public async Task<User> ReadUser(string id)
        {
            var getUser = await _dbContext.Location
                                .Join(_dbContext.User,
                                    location => location.UserId,
                                    user => user.UserId,
                                    (location, user) => user)
                                .Where(user => user.UserId.ToString() == id)
                                .Include(x => x.Address).FirstOrDefaultAsync();
            return getUser;
        }
    }
}
