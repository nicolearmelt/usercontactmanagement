﻿using DR1.Models;
using DR1.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Seeder
{
    public class Seeder
    {
        private readonly IUserService _userService;
        private readonly ILocationService _locationService;

        public Seeder(IUserService userService, ILocationService locationService)
        {
            _userService = userService;
            _locationService = locationService;
        }

        public async Task SeedDatabase()
        {
            if(!_userService.GetAllUsers().Result.Any())
            {
                await SeedUser();
            }
            if (!_locationService.GetAllLocations().Result.Any())
            {
                await SeedLocation();
            }
        }

        public async Task SeedUser()
        {
            using StreamReader file = File.OpenText(Directory.GetCurrentDirectory() + @"\Seeder\user.json");
            using JsonTextReader reader = new JsonTextReader(file);
            var users = JsonConvert.DeserializeObject<List<User>>(file.ReadToEnd());
            for (int i = 0; i < users.Count; i++)
            {
                await _userService.AddUser(new User
                {
                    FirstName = users[i].FirstName,
                    LastName = users[i].LastName,
                    Email = users[i].Email,
                    MobileNumber = users[i].MobileNumber,
                    Gender = users[i].Gender
                });
            }
        }

        public async Task SeedLocation()
        {
            using StreamReader file = File.OpenText(Directory.GetCurrentDirectory() + @"\Seeder\location.json");
            using JsonTextReader reader = new JsonTextReader(file);
            var locations = JsonConvert.DeserializeObject<List<Location>>(file.ReadToEnd());
            for (int i = 0; i < locations.Count; i++)
            {
                await _locationService.AddLocation(new Location 
                {
                   Address = locations[i].Address,
                   City = locations[i].City,
                   PostalCode = locations[i].PostalCode?? new Random().Next(0, 1000000).ToString("D6"),
                   Notes = locations[i].Notes,
                   AddressType = locations[i].AddressType,
                   IsDefault = true,
                   UserId = locations[i].UserId
                });
            }
        }
    }
}
