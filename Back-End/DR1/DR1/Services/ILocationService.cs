﻿using DR1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Services
{
    public interface ILocationService
    {
        Task<List<Location>> GetAllLocations();
        Task<Location> GetLocationById(string id);
        Task<Location> AddLocation(Location location);
        Task<Location> EditLocation(Location location);
        Task<bool> DeleteLocation(string id);
    }
}
