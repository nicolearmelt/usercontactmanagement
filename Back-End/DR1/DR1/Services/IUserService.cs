﻿using DR1.DTO;
using DR1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Services
{
    public interface IUserService
    {
        Task<List<UserAddressDTO>> GetAllUsers();
        User GetUserById(string id);
        Task<User> AddUser(User user);
        Task<User> EditUser(User user);
        Task<bool> DeleteUser(string id);
    }
}
