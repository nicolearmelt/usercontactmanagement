﻿using DR1.Models;
using DR1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Services
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;
        public LocationService(ILocationRepository locationRepository)
        {
            _locationRepository = locationRepository;
        }
        public async Task<Location> AddLocation(Location location)
        {
            return await _locationRepository.CreateLocation(location);
        }

        public async Task<bool> DeleteLocation(string id)
        {
            return await _locationRepository.DeleteLocation(id);
        }

        public async Task<Location> EditLocation(Location location)
        {
            return await _locationRepository.UpdateLocation(location);
        }

        public async Task<List<Location>> GetAllLocations()
        {
            return await _locationRepository.ReadLocations();
        }

        public Task<Location> GetLocationById(string id)
        {
            throw new NotImplementedException();
        }
    }
}
