﻿using AutoMapper;
using DR1.DTO;
using DR1.Models;
using DR1.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DR1.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ILocationRepository _locationRepository;
        private readonly IMapper _mapper;
        public UserService(IUserRepository userRepository, ILocationRepository locationRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _locationRepository = locationRepository;
            _mapper = mapper;
        }

        public async Task<User> AddUser(User user)
        {
            return await _userRepository.CreateUser(user);
        }

        public async Task<bool> DeleteUser(string id)
        {
            return await _userRepository.DeleteUser(id);
        }

        public async Task<User> EditUser(User user)
        {
            return await _userRepository.UpdateUser(user);
        }

        public async Task<List<UserAddressDTO>> GetAllUsers()
        {
            var users = _userRepository.ReadUsers().Result.ToList();

            var usersToReturn = _mapper.Map<ICollection<UserAddressDTO>>(users).ToList();
            //for (int i = 0; i < user.Count; i++)
            //{
            //    var address = new UserAddressDTO();
            //    address.UserId = user[i].UserId;
            //    address.Email = user[i].Email;
            //    address.FullName = user[i].FirstName + " " + user[i].LastName;
            //    address.Gender = user[i].Gender;
            //    address.MobileNumber = user[i].MobileNumber;
            //    for (int x = 0; x < user[i].Address.Count; x++)
            //    {
            //        var billing = user[i].Address.Where(x => x.AddressType == AddressType.Billing).FirstOrDefault();
            //        address.BillingAddress = billing;
            //        address.DeliveryAddress = user[i].Address.Where(x => x.AddressType == AddressType.Delivery).FirstOrDefault();
            //    }
            //    UserAddress.Add(address);
            //}
            return usersToReturn;
        }

        public User GetUserById(string id)
        {
            var getUser = _userRepository.ReadUser(id).Result;

            return getUser;
        }
    }
}
